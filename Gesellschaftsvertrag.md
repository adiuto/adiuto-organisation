---
title: Gesellschaftsvertrag der adiuto.org gUG (haftungsbeschränkt)
lang: de-DE-1901
csl: Novanimus.csl
output:
  custom_document:
    path: /Gesellschaftsvertrag.odt
    toc: true
number_sections: no
papersize: a4  

---

\newpage{}

# § 1 Firma / Sitz

(1) Die Firma der Gesellschaft lautet

adiuto.org gUG (haftungsbeschränkt)

(2) Der Sitz der Gesellschaft ist Berlin.

# § 2 Zweck der Gesellschaft

(1) Die Gesellschaft verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts »Steuerbegünstigte Zwecke« der Abgabenordnung.

(2) Der Zweck der Gesellschaft ist:
	a) die Förderung der Hilfe für politisch, rassistisch oder religiös Verfolgte, für Flüchtlinge, Vertriebene, Aussiedler, Spätaussiedler, Kriegsopfer, Kriegshinterbliebene, Kriegsbeschädigte und Kriegsgefangene, Zivilbeschädigte und Behinderte sowie Hilfe für Opfer von Straftaten; Förderung des Andenkens an Verfolgte, Kriegs- und Katastrophenopfer; Förderung des Suchdienstes für Vermisste, Förderung der Hilfe für Menschen, die auf Grund ihrer geschlechtlichen Identität oder ihrer geschlechtlichen Orientierung diskriminiert werden;
	c) die Förderung der Entwicklungszusammenarbeit;
	d) die Förderung des bürgerschaftlichen Engagements zugunsten gemeinnütziger, mildtätiger und kirchlicher Zwecke;
	e) die Förderung von Kunst und Kultur;
	f) die Förderung von Wissenschaft und Forschung;
	g) die Förderung der Erziehung, Volks- und Berufsbildung einschließlich der Studentenhilfe;

(3) Der Satzungszweck wird verwirklicht insbesondere durch die folgenden Tätigkeiten:

- Das Einwerben, Koordinieren und Verteilen von Sach-, Geld- und Zeitspenden.
- Durch die Entwicklung von Open-Source-Software.
- Wissenschaftliche Begleitung und Auswertung der eigenen Maßnahmen.
- Zusammenarbeit mit anderen Initiativen, Vereinen, Stiftungen etc. im In- und Ausland, die ebenfalls in dem Bereich des Zwecks der Gesellschaft aktiv sind, sofern es sich um steuerbegünstigte Körperschaften oder Körperschaften des öffentlichen Rechts handelt;
- Ehrenamtsqualifizierung und -vernetzung.
- Förderung der Bildung, um die Integration von Geflüchteten und anderen sozial benachteiligten Gruppen zu fördern.

(5) Die Gesellschaft ist berechtigt, im Rahmen der gesetzlichen Vorschriften und der Bestimmungen dieses Gesellschaftsvertrages alle Geschäfte und Rechtshandlungen vorzunehmen, die zur Erreichung des Gesellschaftszwecks dienlich sind oder das Unternehmen zu fördern geeignet erscheinen, insbesondere sich unmittelbar und mittelbar an anderen Unternehmen zu beteiligen, die ebenfalls ausschließlich und unmittelbar gemeinnützige Zwecke verfolgen.

# § 3 Stammkapital/-einlagen

(1) Das Stammkapital der Gesellschaft beträgt € 600,00.
Es ist eingeteilt in 600 Geschäftsanteile zu je 1,00 Euro.

(2) Die Stammeinlagen sind sofort in voller Höhe einzuzahlen.

# § 4 Selbstlosigkeit; Mittelverwendung

Die Gesellschaft ist selbstlos tätig. Sie verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.

Die Mittel der Gesellschaft dürfen nur für ihre satzungsmäßigen Zwecke verwendet werden. Die Gesellschafter dürfen keine Gewinnanteile und in ihrer Eigenschaft als Gesellschafter auch keine sonstigen Zuwendungen aus Mitteln der Körperschaft erhalten.

Die Gesellschafter erhalten bei Auflösung der Gesellschaft oder bei ihrem Ausscheiden aus der Gesellschaft oder bei Wegfall steuerbegünstigter Zwecke nicht mehr als ihre eingezahlten Kapitalanteile und den gemeinen Wert ihrer geleisteten Sacheinlagen zurück.

Es darf keine Person durch Ausgaben, die dem Zweck der Gesellschaft fremd sind, oder durch unverhältnismäßig hohe Vergünstigungen begünstigt werden.

# § 5 Vermögensbindung

Bei Auflösung der Gesellschaft oder beim Wegfall der steuerbegünstigten Zwecke fällt das Vermögen der Gesellschaft, soweit es die eingezahlten Kapitalanteile der Gesellschafter und den gemeinen Wert der von den Gesellschaftern geleisteten Sacheinlagen übersteigt, an eine juristische Person des öffentlichen Rechts oder eine andere steuerbegünstigte, ausschließlich und unmittelbar gemeinnützige Zwecke verfolgende, Körperschaft zwecks Verwendung für die Förderung der Erziehung, Volks- und Berufsbildung und der Förderung von Kunst und Kultur.

Beschlüsse über die Änderung dieses Paragraphen dürfen nur in Abstimmung mit dem zuständigen Finanzamt durchgeführt werden.

# § 6 Geschäftsjahr

Die Gesellschaft wird auf unbestimmte Dauer eingegangen.

Das Geschäftsjahr ist das Kalenderjahr.

# § 7 Geschäftsführung

Die Gesellschaft wird durch einen Geschäftsführer allein vertreten, auch wenn er nicht alleiniger Geschäftsführer ist, es sei denn die Gesellschafterversammlung hat den Geschäftsführern das Recht der Einzelvertretung ausdrücklich entzogen. In diesem Fall wird die Gesellschaft durch zwei Geschäftsführer oder durch einen Geschäftsführer gemeinschaftlich mit einem Prokuristen vertreten.

Ist ein Geschäftsführer dauerhaft von der Ausübung seiner Tätigkeit als Geschäftsführer ausgeschlossen, ruft der übrige Geschäftsführer die Gesellschafterversammlung ein, um einen neuen Geschäftsführer nach Abs. 1 dieses § 5 zu bestellen.

Die Gesellschafterversammlung kann alle, mehrere oder einzelne Geschäftsführer von den Beschränkungen des § 181 BGB befreien. Gleichfalls kann den Geschäftsführern Einzelvertretungsbefugnis erteilt werden.

Die Geschäftsführung erstreckt sich auf alle Handlungen und Rechtsgeschäfte, die der Geschäftsbetrieb mit sich bringt und welche zur Erreichung des Gesellschaftszwecks erforderlich erscheinen.

Die Geschäftsführer sind an Weisungen der Gesellschafterversammlung gebunden. Dies gilt insbesondere für die Vornahme folgender Rechtshandlungen:

- Erwerb von Grundstücken und grundstücksgleichen Rechten
- Veräußerung oder Teilveräußerung des Geschäftsbetriebes
- Geschäfte außerhalb des Geschäftszwecks
- Übernahme eines fremden Geschäftsbetriebes oder Beteiligungen an anderen Unternehmen
- Die Gesellschafterversammlung kann jederzeit eine Geschäftsordnung für die Geschäftsführung aufstellen.

# § 8 Gesellschafterversammlung

Eine Gesellschafterversammlung ist einzuberufen, wenn dies nach dem Gesetz oder dem Wortlaut dieser Satzung erforderlich ist, ferner wenn die Einberufung aus sonstigen Gründen im Interesse der Gesellschaft liegt, jedoch mindestens einmal im Jahr.

Die Einberufung der Gesellschafterversammlung obliegt der Geschäftsführung. Sind mehrere Geschäftsführer bestellt, so ist die Einberufung durch einen der Geschäftsführer ausreichend. Begehren Gesellschafter die Einberufung einer Gesellschafterversammlung, so gilt § 50 GmbHG mit der Maßgabe, dass die Versammlung innerhalb von drei Wochen nach Absendung (Datum des Poststempels) des Begehrens einberufen werden muss.

Zu den Gesellschafterversammlungen sind alle Gesellschafter schriftlich an die letztbekannte Adresse zu laden. Die Ladung hat mit einer Frist von zwei Wochen unter Angabe der Tagesordnung zu erfolgen. Für die Rechtzeitigkeit der Ladung ist das Datum des Poststempels oder des Fax-Sendeprotokolls entscheidend. Auf die Einhaltung dieser Formalien können die Gesellschafter durch Erklärung gegenüber der Geschäftsführung verzichten.

Jeder Gesellschafter kann sich in der Gesellschafterversammlung durch einen Mitgesellschafter oder einen Angehörigen eines steuer- oder rechtsberatenden Berufes vertreten lassen.

Die Gesellschafterversammlung wird von einem aus ihrer Mitte zu wählenden Versammlungsleiter geleitet, der für eine ordnungsgemäße Protokollierung der Beschlüsse Sorge zu tragen hat. Das Beschlussprotokoll ist sämtlichen Gesellschaftern spätestens vier Wochen nach der Gesellschafterversammlung zu übersenden.

Je EUR 1,- der übernommenen Stammeinlage gewährt eine Stimme.

Die Gesellschafterversammlung ist beschlussfähig, wenn mindestens 51% des Stammkapitals vertreten sind. Ist eine Gesellschafterversammlung insoweit nicht beschlussfähig, so ist unter Beachtung der Vorschrift des Abs. 3 zu einer neuen Gesellschafterversammlung mit gleicher Tagesordnung zu laden, welche ohne Rücksicht auf die Zahl der Erschienenen und die Höhe des vertretenen stimmberechtigten Kapitals beschlussfähig ist. Auf diesen Umstand ist in der erneuten Ladung hinzuweisen.

Soweit das Gesetz nicht entgegensteht, ist die Beschlussfassung auch im schriftlichen Verfahren einschließlich Telefax möglich. Auch eine derartige Beschlussfassung ist vom Versammlungsleiter der vorangegangenen Gesellschafterversammlung, hilfsweise vom Initiator der Beschlussfassung, zu protokollieren und den Gesellschaftern unverzüglich abschriftlich zu übersenden.

Die Gesellschafterbeschlüsse werden, soweit nicht im Gesetz oder nach dieser Satzung andere Mehrheiten vorgesehen sind, mit einfacher Mehrheit des vertretenen stimmberechtigten Kapitals gefasst. Für folgende Beschlüsse bedarf es einer Mehrheit von 3/4 des stimmberechtigten Kapitals:

Beschlüsse gemäß § 5 Abs. 4 dieses Gesellschaftsvertrages;

Kapitalerhöhungen oder Kapitalherabsetzungen;

Umwandlungsrechtliche Maßnahmen, insbesondere Verschmelzungen und Abspaltungen;

Abschluss von Unternehmensverträgen, insbesondere Gewinnabführungs- und/oder Beherrschungsverträgen;

Änderungen des Gesellschaftszwecks;

Bestellung und Abberufung von Geschäftsführern;

Sitzverlegung ins Ausland;

Liquidation der Gesellschaft.

Die Anfechtung von Gesellschafterbeschlüssen durch Klageerhebung ist nur innerhalb einer Frist von einem Monat nach Absendung der Abschrift des Gesellschafterbeschlusses zulässig.

# § 9 Wirtschaftsplan

Die Geschäftsführung stellt rechtzeitig für jedes Geschäftsjahr einen Wirtschaftsplan und eine der Wirtschaftsführung zugrunde zu legende fünfjährige Finanzplanung auf, die den Gesellschaftern zur Kenntnis gebracht wird, so dass die Gesellschafterversammlung vor Beginn des Geschäftsjahres ihre Zustimmung erteilen kann. Der Wirtschaftsplan umfasst den Erfolgsplan, den Vermögensplan und die Stellenübersicht. Die Geschäftsführer unterrichten den Aufsichtsrat mindestens halbjährlich über die Entwicklung der Geschäftslage, darüber hinaus nach Bedarf unverzüglich, insbesondere bei erfolgsgefährdenden Mindererträgen oder Mehraufwendungen.

# § 10 Treupflichten; Wettbewerbsverbot

Die Gesellschafter sind verpflichtet, über alle Angelegenheiten der Gesellschaft, insbesondere über den Stand und Gang der Geschäfte, über Kunden sowie über Kalkulationen und Jahresabschlüsse, gewerbliche Schutzrechte und Know-how, Stillschweigen zu bewahren. Die Gesellschaft betreffende Unterlagen dürfen nicht in die Hände unberechtigter Dritter gelangen. Diese Verpflichtungen gelten auch nach dem Ausscheiden eines Gesellschafters aus der Gesellschaft.

Die Gesellschafter sind nicht berechtigt während der Dauer der Gesellschaft mit einem Konkurrenten der Gesellschaft zusammenzuarbeiten. Konkurrenten im Sinne dieser Konkurrenzschutzklausel sind andere, mit der Gesellschaft vergleichbare Unternehmen, die den gleichen oder einen ähnlichen Unternehmenszweck verfolgen oder die aufgrund ihrer Tätigkeit geeignet sind, das Erreichen des Unternehmenszwecks für die Gesellschaft zu verhindern oder zu erschweren. Für jeden Fall der Zuwiderhandlung gegen vorstehende Konkurrenzklausel, ist der jeweilige Gesellschafter zur Leistung von Schadensersatz verpflichtet. Darüberhinaus hat der Gesellschafter seinen durch die Konkurrenzhandlung erwirtschafteten Umsatz an die Gesellschaft herauszugeben.

Die Geschäftsführer und/oder Gesellschafter können von dem Verbot des Wettbewerbs mit der Gesellschaft ganz oder teilweise durch Gesellschafterbeschluss entbunden werden. Der Gesellschafterbeschluss ist mit einfacher Mehrheit zu fassen. Der vom Wettbewerbsverbot Befreite hat dabei kein Stimmrecht, es sei denn, alle Anteile sind zwischenzeitlich in seiner Hand vereinigt oder alle Gesellschafter sollen von der Treuepflicht entbunden werden. Über ein mögliches Entgelt zur Abgeltung der Befreiung von dem vertraglichen Wettbewerbsverbot befindet die Gesellschafterversammlung im jeweiligen Fall.

# § 11 Jahresabschluss, Gewinnverteilung

Der Jahresabschluss ist von dem oder den Geschäftsführern bis zum 31. März des Folgejahres aufzustellen, zu unterzeichnen und unverzüglich der Gesellschafterversammlung zur Feststellung des Jahresabschlusses zuzuleiten.

Sofern nicht eine Prüfung des Jahresabschlusses durch einen Abschlussprüfer gem. §§ 316 ff. HGB zwingend vorgeschrieben ist, kann der Jahresabschluss aufgrund eines mit einfacher Mehrheit zu fassenden Beschlusses der Gesellschafterversammlung von einem von dieser Mehrheit zu bestellenden Wirtschaftsprüfer oder vereidigten Buchprüfer auf Kosten der Gesellschaft geprüft werden.

Die Gesellschafterversammlung stellt innerhalb von zwei Monaten nach Vorlage des Jahresabschlusses durch die Geschäftsführer den Jahresabschluss fest und beschließt nach freiem Ermessen die Verwendung des jährlichen Reingewinns, wobei auch freie Rücklagen gebildet werden können. Der Gewinnverwendungsbeschluss wird mit einer Mehrheit von 3/4 des stimmberechtigten Kapitals gefasst.

# § 12 Verfügung über Geschäftsanteile

Jede Teilung, Vereinigung, entgeltliche oder unentgeltliche Veräußerung oder Abtretung von Geschäftsanteilen sowie jede Sicherungsübertragung, Verpfändung oder sonstige Belastung von Geschäftsanteilen einschließlich der Bestellung eines Nießbrauchs ist nur mit einstimmiger Zustimmung der Gesellschafterversammlung zulässig, dies gilt nicht für die Übertragung von Geschäftsanteilen eines Gesellschafters an mit ihm konzernverbundene Unternehmen, sofern diese der aktienrechtlichen Beherrschung unterworfen sind.

Will ein Gesellschafter seinen Geschäftsanteil ganz oder teilweise auf einen Dritten übertragen, so hat er den Anteil zunächst den übrigen Gesellschaftern zum Kauf anzubieten. Für die Ausübung dieses Erwerbsrechts gelten die §§ 504 ff. BGB sinngemäß mit der Maßgabe, dass das Entgelt gem. § 18 dieses Gesellschaftsvertrages zu bestimmen ist, wobei der Höchstpreis der mit dem Dritten vereinbarte Preis ist. Soll die Veräußerung unentgeltlich vorgenommen werden, so wird das Entgelt gemäß § 18 dieses Gesellschaftsvertrages um 10 % ermäßigt. Den übrigen Gesellschaftern steht das Erwerbsrecht im Verhältnis ihrer bisherigen Beteiligung an der Gesellschaft zu. Macht ein Gesellschafter von seinem Erwerbsrecht keinen Gebrauch, so wächst das Erwerbsrecht den übrigen Gesellschaftern im entsprechenden Verhältnis zu.

Die Veräußerung an einen Dritten darf erst erfolgen wenn und soweit die erwerbsberechtigten Gesellschafter von ihrem Erwerbsrecht nicht innerhalb einer Frist von acht Wochen nach Benachrichtigung durch den veräußerungswilligen Gesellschafter von der Absicht der unentgeltlichen oder entgeltlichen Veräußerung seines Geschäftsanteils an einen Dritten Gebrauch gemacht oder auf ihr Erwerbsrecht verzichtet haben. Abs. 1 bleibt unberührt.

# § 13 Einziehung von Geschäftsanteilen

Die Einziehung von Geschäftsanteilen ist mit Zustimmung des betroffenen Gesellschafters jederzeit zulässig.

Die Zwangseinziehung eines Geschäftsanteils ist auch dann zulässig, wenn

ein Gesellschafter seine Gesellschafterpflicht grob verletzt oder in seiner Person ein anderer wichtiger Grund vorliegt;

über das Vermögen eines Gesellschafters das Insolvenzverfahren eröffnet wird oder mangels Masse die Eröffnung abgelehnt worden ist;

von Seiten eines Gläubigers eines Gesellschafters Zwangsvollstreckungsmaßnahmen in dessen Geschäftsanteil vorgenommen werden und es dem Betroffen nicht binnen eines Monats seit Beginn der Maßnahme gelungen ist, ihre Aufhebung zu erreichen. Bei Zwangsvollstreckungsmaßnahmen kann die Gesellschaft den vollstreckenden Gläubiger befriedigen, wobei der betroffene Gesellschafter der Befriedigung nicht widersprechen kann;

ein Gesellschafter verstirbt;

Die Einziehung erfolgt durch Beschluss der Gesellschafterversammlung ohne Mitwirkung des betroffenen Gesellschafters mit einfacher Mehrheit.

Statt der Einziehung kann die Gesellschaft bestimmen, dass der betroffene Geschäftsanteil an sie selbst oder an eine von ihr zu benennende natürliche oder juristische Person abgetreten wird. Abs. 3 gilt sinngemäß.


# §14 Austritt aus der Gesellschaft

Jeder Gesellschafter kann seinen Austritt aus der Gesellschaft erklären, bei Vorliegen eines wichtigen Grundes ohne Frist, ansonsten mit einer Frist von drei Monaten zum Ende eines Geschäftsjahres, jedoch frühestens zum 1. Januar 2023. Jede Austrittserklärung hat mittels eingeschriebenen Briefes an die Geschäftsführer zu erfolgen, wobei für eine Rechtzeitigkeit der Austrittserklärung das Datum des Poststempels maßgeblich ist.

Die Gesellschaft ist berechtigt, den Geschäftsanteil des ausscheidenden Gesellschafters einzuziehen oder die Abtretung an eine von ihr zu benennende natürliche oder juristische Person zu verlangen. § 18 gilt entsprechend.

Zwischen Austrittserklärung und Vollendung der Einziehung bzw. der Abtretung ruhen die Rechte des austretenden Gesellschafters.

# §15 Einziehungsentgelt

In jedem Fall des Ausscheidens eines Gesellschafters aus der Gesellschaft und
für die Fälle der Anteilsveräußerung an einen oder mehrere Gesellschafter oder
an die Gesellschaft ist wie folgt zu verfahren:
Die Gesellschafter erhalten bei ihrem Ausscheiden oder bei Auflösung der Gesellschaft nicht mehr als die von ihnen eingezahlten Kapitalanteile und den gemeinen
Wert ihrer geleisteten Sacheinlagen zurück.

Eventuelle spätere nachträgliche Änderungen dieses Wertes bleiben in jedem
Falle unberücksichtigt.

Sollte über die zu erfolgende Bewertung des Geschäftsanteiles unter den Gesellschaftern ein Einvernehmen nicht erzielt werden, ist ein Gutachten eines öffentlich
bestellten Wirtschaftsprüfers einzuholen, der darin die Bewertung für alle Beteiligte verbindlich vorzunehmen hat. Der Wirtschaftsprüfer ist von allen Gesellschaftern auszuwählen. Andernfalls ist er auf Antrag eines Gesellschafters von der
Sitz der Gesellschaft zuständigen Industrie- und Handelskammer zu bestimmen.
Die durch dieses Verfahren ausgelösten Kosten trägt der ausscheidende Gesellschafter.

Abs. 1 bis Abs. 8 gelten entsprechend für die Abtretung von Geschäftsanteilen gem. § 10 Abs. 2, § 11 Abs. 4 und § 12 Abs. 2.

# § 16 Liquidation der Gesellschaft

Die Auflösung der Gesellschaft erfolgt in den gesetzlich bestimmten Fällen.

Der Beschluss der Gesellschafterversammlung über die Auflösung der Gesellschaft kann nur einstimmig gefasst werden.

Die Liquidation erfolgt durch die Geschäftsführer oder einen oder mehrere von der Gesellschafterversammlung bestimmte Liquidatoren.

# § 17 Schlussbestimmungen

Die Gründungskosten, insbesondere Rechtsanwalts-, Notariats- und Steuerberaterkosten für Beratung, Vorbereitung und Durchführung der Beurkundung des Gesellschaftsvertrages und für die Anmeldung im Handelsregister sowie evtl. anfallende Steuern tragen

werden bis zum Betrag von 600 Euro von der Gesellschaft getragen.

Sollten eine oder mehrere Bestimmungen dieses Vertrages gegen ein gesetzliches Verbot verstoßen oder aus anderen Gründen nichtig oder unwirksam sein, bleibt dadurch die Wirksamkeit des übrigen Vertrages unberührt. Die nichtige oder unwirksame Bestimmung ist durch die der nichtigen oder unwirksamen Bestimmung am nächsten kommende wirksame Bestimmung zu ersetzen.

Bekanntmachungen der Gesellschafter erfolgen elektronisch im Bundesanzeiger.

Eine Änderung dieses Gesellschaftsvertrages ist dem zuständigen Finanzamt anzuzeigen und mit diesem vor der Rechtswirksamkeit der Änderung abzustimmen. Die neue Fassung dieses Gesellschaftsvertrages ist beim Handelsregister einzureichen.
